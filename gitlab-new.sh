#!/usr/bin/env bash

if [! "$(grep gitlab ~/.gitconfig)" ]; then
	echo -n "Enter your name: "
	read name

	echo -n "Enter your email address: "
	read email_addr

	echo -n "Enter your gitlab username: "
	read gitlab_name

	git config --global user.name "$name"
	git config --global user.gitlab "$gitlab_name"
	git config --global user.email "$email_addr"
fi

function gpl_clone {
	wget https://www.gnu.org/licenses/gpl-3.0.md &>/dev/null && mv gpl-3.0.md LICENSE.md
	git add *
	git commit -m "Add LICENSE.md"
}

[[ -d ".git" ]] && rm -rf .git
[[ -d ".git" ]] || git init &> /dev/null

REPO=`git rev-parse --show-toplevel | sed -r 's/.*\/(.*)$/\1/g'`
NAME=`grep gitlab ~/.gitconfig | cut -d ' ' -f 3`

[[ $(grep 'origin' .git/config) ]] || git remote add origin git@gitlab.com:$NAME/$REPO.git

[[ $(ls | sed -rn '/LICENSE/p') ]] || gpl_clone

[[ ! $(git status -suno) ]] && git add * && git commit -m "initial commit"

git push --set-upstream git@gitlab.com:$NAME/$REPO.git
