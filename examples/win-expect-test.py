#!/usr/bin/env python3
import os
import sys
from sys import stdout
import pexpect
from pathlib import Path
import subprocess
from pexpect import popen_spawn
import time

def listen():

    passwrd = "mypass"
    uname = "myuser"
    pexpt = pexpect.popen_spawn.PopenSpawn('PowerShell', encoding='utf8')

    time.sleep(2)
    pexpt.sendline('-Command Read-Host "Enter password"')
    time.sleep(2)
    pexpt.expect('Enter password')
    time.sleep(2)
    pexpt.sendline("mypass")

    pexpt.expect('Enter username')
    pexpt.sendline(uname)


def main():
    cmds=[
        '-Command Read-Host "Enter password"',
        '-Command Read-Host "Enter username"'
    ]
    for cmd in cmds:
        listen(cmd)


#subprocess.run(['powershell', '-Command', 'Read-Host "enter words"'], shell=True)#stdout=open(hello-mac.log, 'a'), stderr=open(hello-mac.err, 'a'))
#main()

listen()

# c = pexpect.popen_spawn.PopenSpawn('PowerShell', encoding='utf-8')
# time.sleep(2)
# c.sendline('Start-Process PowerShell -Verb RunAs')
# time.sleep(2)
# c.expect('PS.*>', 10)
# time.sleep(2)
# c.sendline("Get-WindowsCapability -Online | Where-Object Name -like 'OpenSSH*'")
