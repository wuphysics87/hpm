#!/usr/bin/env python3
import shutil
import os
from pathlib import Path
import subprocess


array = [
        'Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString(\'https://community.chocolatey.org/install.ps1\'))',
        'choco feature enable -n=allowGlobalConfirmation',
        'choco install -y --nocolor -r python --version=3.11.0',
        'choco install -y --nocolor -r dos2unix',
        'choco install -y --nocolor -r nodejs fantasque-sans.font shellcheck microsoft-windows-terminal git vscodium cascadia-code-nerd-font nerd-fonts-3270 nerdfont-hack cmake make mingw youtube-dl',

        'python.exe -m ensurepip',
        'python.exe -m pip install --upgrade pip',
        'python.exe -m setuptools install --upgrade setuptools',

        'pip install jedi pycodestyle pipreqs pyinstaller',
        'npm install csslint eslint remark remark-lint',
        'wsl.exe --unregister Ubuntu-22.04',
        'wsl.exe --update',
        'wsl.exe --set-default-version 1',
        'wsl.exe --install Ubuntu-22.04 --no-launch',
        'Rename-Item -Path "C:\ProgramData\Microsoft\Windows\Start` Menu\Programs\VSCodium\VSCodium.lnk" -NewName Codium.lnk',
]

# arch_install = [
#         'if ( ! [System.IO.File]::Exists("$env:USERPROFILE\Downloads\Arch.tar")) {Invoke-WebRequest -Uri https://gitlab.com/wuphysics87/hello-arch/-/raw/main/Arch.tar -OutFile "$env:USERPROFILE\Downloads\Arch.tar"}',
#         'wsl.exe --import hello-arch "C:\Windows\System32" "$env:USERPROFILE\Downloads\Arch.tar"',
# ]


codium_packages=[
    'batisteo.vscode-django',
    'bierner.markdown-mermaid',
    'bierner.markdown-preview-github-styles',
    'christian-kohler.npm-intellisense',
    'davidanson.vscode-markdownlint',
    'dbaeumer.vscode-eslint',
    'donjayamanne.python-environment-manager',
    'donjayamanne.python-extension-pack',
    'ecmel.vscode-html-css',
    'esbenp.prettier-vscode',
    'formulahendry.code-runner',
    'foxundermoon.shell-format',
    'gitlab.gitlab-workflow',
    'hookyqr.beautify',
    'ionutvmi.path-autocomplete',
    'jeff-hykin.better-shellscript-syntax',
    'jeronimoekerdt.color-picker-universal',
    'kevinrose.vsc-python-indent',
    'mads-hartmann.bash-ide-vscode',
    'magicstack.magicpython',
    'manuth.eslint-language-service',
    'ms-python.isort',
    'ms-python.python',
    'ms-toolsai.jupyter',
    'ms-toolsai.jupyter-keymap',
    'ms-toolsai.jupyter-renderers',
    'ms-toolsai.vscode-jupyter-cell-tags',
    'ms-toolsai.vscode-jupyter-slideshow',
    'ms-vscode.theme-markdownkit',
    'njpwerner.autodocstring',
    'phoenisx.cssvar',
    'pinage404.bash-extension-pack',
    'streetsidesoftware.code-spell-checker',
    'sugatoray.vscode-markdown-extension-pack',
    'tht13.python',
    'timonwong.shellcheck',
    'tomoki1207.pdf',
    'vunguyentuan.vscode-css-variables',
    'wholroyd.jinja',
    'yzhang.markdown-all-in-one',
    'xabikos.javascriptsnippets',
    'yzane.markdown-pdf']
