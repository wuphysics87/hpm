#!/usr/bin/env python3
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton,QLineEdit,QFormLayout,QLabel,QGridLayout,QProgressBar,QVBoxLayout,QHBoxLayout
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot, QObject, pyqtSignal

from modules.dotfiles import install_dotfiles as dot_install
from modules.install import array as i_array, codium_packages as cpac

import ctypes

import os, subprocess, shutil, time, sys
from subprocess import PIPE, Popen
from threading import *
import pathlib


def Main():

    for cmd in i_array:
        ex.setUpdatesEnabled(False)
        ex.update_status('Run: {}'.format(cmd))
        ex.setUpdatesEnabled(True)
        subprocess.run('powershell -Command {}'.format(cmd), shell=True)#stdout=open(hello-mac.log, 'a'), stderr=open(hello-mac.err, 'a'))

    # for cmd in arch_install:
    #     ex.setUpdatesEnabled(False)
    #     ex.update_status('Downloading Hello-Arch. This takes time on slower connections.')
    #     ex.setUpdatesEnabled(True)
    #     subprocess.run('powershell -Command {}'.format(cmd), shell=True)#stdout=open(hello-mac.log, 'a'), stderr=open(hello-mac.err, 'a'))

    for pac in cpac:
        ex.setUpdatesEnabled(False)
        ex.update_status('codium --install {}'.format(pac))
        ex.setUpdatesEnabled(True)
        subprocess.run(['powershell', '-Command', 'codium --install-extension {} --force'.format(pac)], shell=True)#stdout=open(hello-mac.log, 'a'), stderr=open(hello-mac.err, 'a'))

    ex.setUpdatesEnabled(False)
    ex.update_status("Installation Complete! You May Close This Window.")
    ex.setUpdatesEnabled(True)

    dot_install()
    # my_dir=(pathlib.Path().resolve()).split(':')
    # print(my_dir.replace('\\','/'))
    subprocess.run(['wsl.exe -d Ubuntu-22.04 -u root win-install.bash'])


class App(QWidget):

    def __init__(self):
        super().__init__()
        self.pbar_status = 0
        self.title = 'Hello Installer!'
        self.left = 500
        self.top = 500
        self.width = 800
        self.height = 250
        self.setMaximumWidth(800)

        self.current_command = QLabel("Welcome! Press 'Install' to Continue", self)
        self.current_command.setMaximumWidth(750)

        self.pbar=QProgressBar(self)
        self.pbar.setMaximum(58)
        self.pbar.setFormat("%p%")

        self.installbutton = QPushButton("Install")

        outer_layout = QVBoxLayout()

        status_layout = QGridLayout()
        status_layout.addWidget(self.current_command,0,0)
        status_layout.addWidget(self.pbar,1,0)
        outer_layout.addLayout(status_layout)

        input_layout = QHBoxLayout()
        input_layout.addWidget(self.installbutton)
        outer_layout.addLayout(input_layout)

        self.setLayout(outer_layout)

        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.installbutton.clicked.connect(self.on_click)

        self.show()

    def update_status(self,cmd):
        self.current_command.setText(str(cmd))
        self.current_command.update()
        self.pbar_status+=1
        self.pbar.setValue(self.pbar_status)

    def thread(self):
        t1=Thread(target=self.main_program)
        t1.start()

    def main_program(self):
        Main()

    def error_output (self,cmd):
        self.current_command.setText(str(cmd))
        self.current_command.update()

    @pyqtSlot()
    def on_click(self):

        if(True):
            try:
                self.installbutton.setEnabled(False)
                self.thread()
            except:
                self.error_output("Error: Install Failed.  See error.log for details.")

def is_admin():
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return False

def rerun_as_admin():
    ctypes.windll.shell32.ShellExecuteW(
        None,
        u"runas",
        sys.executable,
        __file__,
        None,
        1
    )

if __name__ == '__main__':
    if is_admin():
        app = QApplication(sys.argv)
        ex = App()
        sys.exit(app.exec_())
    else:
        rerun_as_admin()
