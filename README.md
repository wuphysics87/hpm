# Hello Package Manager

This is the package manager and the installers for the Hello World coding project!

# Mac 
Open a terminal and clone this repository with:

``` sh
git clone https://gitlab.com/wlu-hello-world/hello-py-install
```

navigate into the root directory of the repo (when you `ls` you'll see the following)
![image](images/mac-1.png)

run:

``` sh
pip install -r requirements.txt
```

then 
```
cd mac
```

finally,

``` sh
python Installer.py
```


# Windows

Download this repository as a zip file

![image](images/win-1.png)

Navigate to your Downloads folder and extract it.

![image](images/win-2.png)

Open powershell:

![image](images/win-3.png)

`cd` to your Downloads folder and your extracted folder.  Run 

```
pip install -r requirements.txt
```

`cd` into the `win` directory and run

``` sh
python Installer.py
```

![image](images/win-4.png)

